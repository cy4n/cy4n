# hi!

i am **Chris** and i just found the profile feature. 

quick starter (until i find time to fully fill this)

i have been working in IT for almost 20 yrs, switching back and forth between ops and dev, picking the best of both worlds. 

i currently work as Head of Technology and Architecture (POS) for dmTECH, subsidiary of dm Drogeriemarkt, Germany's most favorite retailer 

i'm a frequent speaker at (mostly German) conferences, and i have covered topics like kubernetes, software security and continuous integration / testing.

i also co-organize the DevOps Meetup Karlsruhe with over 1000 members.
